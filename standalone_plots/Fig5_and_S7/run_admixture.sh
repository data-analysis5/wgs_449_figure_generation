#!/bin/bash

# Run ADMIXTURE for population structure analysis
# Below is an example on the downsampled (10 per population)
# dataset (n = 265) specifically from ./prep_for_admixture.sh 
# referred to as v4b. This is for Figure S7.
# This was repeated for a similar dataset with the top 15
# ethnolinguistic groups from the 54gene dataset, plus select
# populations from 1000 genomes and HGDP. This is referred to
# dataset V5 for main text Figure 5.
plinkfile="../scratch/admixture/v4b"   
for x in 2 3 4 5 6
do
 K=$x
 echo -e "\n\nON K = $K"
 for r in {1..10} 
 do
   echo -e "\n\t\tON r $r"
   admixture -s ${RANDOM} $plinkfile.ped $K -j16
   mv v4b.${K}.Q v4b.K${K}r${r}.Q 
 done
done


# Run pong for visuals for the downsampled population
# dataset generated above. Below is an example for K=6.
# This analysis was repeated for all K groups for both 
# datasets shown in main text Figure 5 and supplemental
# Figure S7.
K=6
rm v4b.k$K.multiplerun.Qfilemap
for r in {1..10} 
do
  awk -v K=$K -v r=$r -v file=v4b.K${K}r${r} 'BEGIN { printf("K%dr%d\t%d\t%s.Q\n",K,r,K,file) }' >> v4b.k$K.multiplerun.Qfilemap
done
ls v4b.k$K.multiplerun.Qfilemap
cat v4b.k$K.multiplerun.Qfilemap
pong -m v4b.k$K.multiplerun.Qfilemap --greedy -s .95 -i v4b.ind2pop
