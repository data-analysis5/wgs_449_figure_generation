#!/bin/bash

# This script contains the code for generating different
# versions of the combined datasets containing populations
# from the 54gene dataset, 1000 Genomes (1KG AFR), and 
# HGDP and subequently running ADMIXTURE on these datasets.

mkdir -p ../scratch/admixture
plinkfile="../scratch/tmp4.prune2.noMHC"

# Different versions of the data:
#	1) N=1354 individuals - 54gene + 1KG AFR + 1KG EUR + HGDP Mozabite and Yoruba (Africa + Admixed Afr + Northern African) (v1)
#	2) N=655 individuals - "Local AFR" - all 54gene + 1KG YRI, ESN + HGDP Yoruba (AFRICA only) (v2)
#  	3) N=948 individuals - "Broad AFR" - all 54gene + 1KG YRI, ESN, GWD, LWK, MSL + HGDP Yoruba (AFRICA only) (v3)
#  	4) N=399 individuals - "Broad AFR" - all 54gene + 1KG YRI, ESN, GWD, LWK, MSL + HGDP Yoruba (AFRICA only) (v4)
#	4b) N=265 - as above but additionally remove smaller 54gene populations, restricting to top 15 (v4b)
#	5) N=443 individuals - all of 54gene plus downsampled 1KG/HGDP....no small 54gene populations (v5)
#	6) N=559 - all of 54gene plus downsampled 1KG/HGDP....all small 54gene populations are included (v6)

# make different files for ADMIXTURE
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --keep ../scratch/keep.1KGEUR_54G_Mozabite.txt --out ../scratch/admixture/v1
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --keep ../scratch/keep.1KGEUR_54G_HGDP_AFRonly.txt --out ../scratch/admixture/v2
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --keep ../scratch/keep.1KGEUR_54G_HGDP_AFR_broad.txt --out ../scratch/admixture/v3
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --keep ../files/merged_54gene_NYGC1kg_HGDP_dataset_ancestries.1KGEUR_54G_Mozabite.max10perGroup.tsv --out ../scratch/admixture/v4 


# Make an exclude file of all the smaller 54gene populations that we want to exclude:
# NOTE: leaving "ESAN" from 54gene in here for comparison to ESN from 1KG
# This dataset will be used to run admixture on downsampled populations
# shown in Main text Figure S7
awk '$2 == "EFIK" || $2 == "TULA" || $2 == "IGALA" || $2 == "EBIRA" || $2 == "MWAHAVUL" || $2 == "BOLE" || $2 == "IKWERRE" || $2 == "URHOBO" || \
$2 == "NGAS" || $2 == "JUKUN" || $2 == "BOKI" || $2 == "BAJJU" || $2 == "TIV" || $2 == "IDOMA" || $2 == "BIASE" || $2 == "ABI" || \
$2 == "YAKURR" || $2 == "ISOKO" || $2 == "GBAGI" || $2 == "EKOI" || $2 == "EJAGHAM" || $2 == "BAKULU" || $2 == "TAROK" || $2 == "ORON" || $2 == "AFIZERE" || \
$2 == "RON" || $2 == "NINZAM" || $2 == "KALABARI" || $2 == "JERE" || $2 == "ETUNG" || $2 == "UNCERTAIN"  || $2 == "ANANG" { print "0",$1 }' \
../files/merged_54gene_NYGC1kg_HGDP_dataset_ancestries.1KGEUR_54G_Mozabite.tsv > ../scratch/exclude.54gene_smallPops.txt
./plink19 --file ../scratch/admixture/v4 --maf 0.01 --remove ../scratch/exclude.54gene_smallPops.txt --recode12 --out ../scratch/admixture/v4b

cut -f 2 -d ' '  ../scratch/admixture/v4b.ped | grep -n '' | tr -s ':' ' ' | awk '{print $2,$1 }' | sort -s -k 1b,1 > v4b.ind.s
cat ../files/merged_54gene_NYGC1kg_HGDP_dataset_ancestries.1KGEUR_54G_Mozabite.tsv | awk '{ print $1"\t"$2 }'  | sort -s -k 1b,1 > v4b.id2pop.s
join v4b.ind.s  v4b.id2pop.s | sort -gk2 | awk '{print $3 }' > v4b.ind2pop


# V5 - all of 54gene plus downsampled 1KG/HGDP....no small 54gene populations			
# N=422, this is for Main text Figure 5
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --maf 0.01 \
	--keep ../scratch/include.559.all54gene_plus_1KGdownsampled \
	--remove ../scratch/exclude.54gene_smallPops.txt \
	--out ../scratch/admixture/v5

# V6 - all of 54gene plus downsampled 1KG/HGDP....all small 54gene populations are included
# N=559, (smaller pops included)
./plink19 --bfile $plinkfile --recode12 --snps-only just-acgt --maf 0.01 \
	--keep ../scratch/include.559.all54gene_plus_1KGdownsampled \
	--out ../scratch/admixture/v6
