#!/usr/bin/env Rscript
# standalone plot
library(ggplot2)
library(stringr)
library(RColorBrewer)
library(dplyr)
library(tidyverse)

cvg.file <- "sample_coverages_bcftools.tsv"
linker.file <- "ancestry_site_linker.tsv"
outfile <- "plot_cvg_by_site_ancestry.png"

make.plot <- function(cvgf, linker, out.file){
  cvg <- read.table(cvgf, sep="\t", header=TRUE, stringsAsFactors = FALSE)
  lnkr <- read.table(linker, sep="\t", header=TRUE, stringsAsFactors = FALSE)
  df <- merge(lnkr, cvg, by="ID", all=TRUE)
  df <- df[!df$ANC == "UNCERTAIN",]
  top.ancs <- df %>% group_by(ANC) %>% tally() %>% top_n(n=15) %>% pull("ANC")
  df <- df[df$ANC %in% top.ancs,]
  colnames(df) <- c("ID", "elg", "Center", "coverage")
  
  my.theme <- theme_light() + 
    theme(plot.title = element_text(size = 16, hjust = 0.5), 
          axis.title = element_text(size = 14), 
          axis.text.y = element_text(size = 12), 
          axis.text.x = element_text(size = 12, angle = -45, hjust = 0), 
          strip.background = element_blank(), 
          strip.text = element_text(size = 16, colour = "black"))
  
  my.plot <- ggplot(aes(x = elg, y = coverage, fill = Center), data = df) + geom_boxplot() + my.theme + xlab("Ethnolinguistic group") + ylab("coverage")
  ggsave(out.file, plot = my.plot, height = 6, width = 8, units = "in")
}

make.plot(cvg.file, linker.file, outfile)
