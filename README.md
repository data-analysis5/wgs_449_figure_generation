# Generate_figures: Whole-genome sequencing across 449 samples spanning 47 ethnolinguistic groups provides insights into genetic diversity in Nigeria

This repository contains a combination of a Snakemake workflow and standalone scripts developed to generate the plots within the paper titled _Whole-genome sequencing across 449 samples spanning 47 ethnolinguistic groups provides insights into genetic diversity in Nigeria_. Please see the details below on the directory structure. 

## Directory Structure

### Snakemake Pipeline
This repository contains a small Snakemake pipeline for the analysis and creation for data in the plots for main text Figure 3. The rules and steps for this pipeline are defined in `workflow/Snakefile`. 

### Standalone Scripts
For the rest of figures within the paper, both from the main text and supplemental text, there are a combination of R and bash scripts in `standalone_plots` and subdirectories for each figure. These scripts are annotated and describe the general steps for generating the plots within the paper. 

While we do not provide the intermediate files (i.e. TSVs, plink files) used within the hardpaths of these scripts within this repository, we describe the analysis methods used to generate these datasets within the paper itself and provide raw data through EGA (see _Data Availability_ section within the paper). 

## Contact

Please contact colm@54gene.com for more details on this repository, the paper, or if you have any questions.