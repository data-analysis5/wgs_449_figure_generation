library(ggplot2)
library(stringr)
library(RColorBrewer)

dbsnp.files <- snakemake@input[["dbsnp_psc"]]
gnomad.files <- snakemake@input[["gnomad_psc"]]
dbsnp.out.file <- snakemake@output[["dbsnp_psc"]]
gnomad.out.file <- snakemake@output[["gnomad_psc"]]

make.plot <- function(in.files, out.file, xtitle) {
    df <- data.frame()
    for(infile in in.files) {
        f <- read.table(infile, sep = "\t", stringsAsFactors = FALSE, header = FALSE)
        ancestries <- stringr::str_extract(f[,1], "[A-Z]+")
        ancestry.counts <- table(ancestries)
        passing.ancestries <- names(ancestry.counts)[ancestry.counts >= 20]
        passing.ancestries <- passing.ancestries[passing.ancestries != "UNCERTAIN"]
        f <- f[ancestries %in% passing.ancestries,]
        ancestries <- ancestries[ancestries %in% passing.ancestries]
        subjects <- f[,3]
        uniq.subjects <- unique(subjects)
        my.subset <- stringr::str_replace(f[1,1], "^.*\\.([a-z]+)_variants.*", "\\1")
        counts <- sapply(uniq.subjects, function(x) {sum(f[subjects == x, c(5, 6)])})
        subject.ancestries <- sapply(uniq.subjects, function(x) {ancestries[subjects == x][1]})
        temp.df <- data.frame("ancestry" = subject.ancestries, "variant.subset" = my.subset, "counts" = counts)

        if (nrow(df) == 0) {
            df <- temp.df
        }
        else {
            df <- rbind(df, temp.df)
        }
    }

    ncolors <- length(unique(df[,"variant.subset"]))
    if(ncolors == 3){
        df[,"variant.subset"] <- factor(df[,"variant.subset"], levels = c("rare", "uncommon", "common"))
    }
    my.theme <- theme_light() + theme(plot.title = element_text(size = 16, hjust = 0.5), axis.title = element_text(size = 14), axis.text.y = element_text(size = 12), axis.text.x = element_text(size = 12, angle = -45, hjust = 0), strip.background = element_blank(), strip.text = element_text(size = 16, colour = "black"))
    my.plot <- ggplot(aes(x = ancestry, y = counts, colour = variant.subset), data = df) + geom_boxplot(position = "dodge") + scale_colour_manual(values = brewer.pal(4, "Dark2")[seq_len(ncolors)])
    my.plot <- my.plot + facet_wrap(vars(variant.subset), ncol = 1, scales = "free_y") + my.theme
    my.plot <- my.plot + xlab(xtitle)
    ggsave(out.file, plot = my.plot, height = 6, width = 8, units = "in")
}

make.plot(dbsnp.files, dbsnp.out.file, "Ethnolinguistic group")
make.plot(gnomad.files, gnomad.out.file, "Ethnolinguistic group")
